const config = require('../config/environment');
const Promise = require('bluebird');
const bcrypt = require('bcrypt');

const encryptPassword = str =>
  new Promise((resolve, reject) => {
    bcrypt.genSalt(config.encryptionRounds, (genErr, salt) => {
      if (genErr) return reject(genErr);
      return bcrypt.hash(str, salt, (err, hash) => {
        if (err) return reject(err);

        return resolve(hash);
      });
    });
  });

const comparePassword = (originalPassword, hashPassword) =>
  new Promise((resolve, reject) => {
    bcrypt.compare(originalPassword, hashPassword, (err, match) => {
      if (err) return reject(err);
      return resolve(match);
    });
  });


module.exports = {
  encryptPassword,
  comparePassword,
};
