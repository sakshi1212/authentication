const config = require('../config/environment');
const jwt = require('jsonwebtoken');

function genAccountToken({ id }) {
  return jwt.sign({ id }, config.secrets.token);
  // TODO - set time on verification link, also add ability to resend verification link
  // , { expiresIn: config.expireTimes.verificationToken }
}

function genTokenFromId(id) {
  return jwt.sign({ id }, config.secrets.token, { expiresIn: config.expireTimes.token });
}

function verifyAccountToken(userId, token) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, config.secrets.token, (err, decoded) => {
      if (err) {
        if (err.name === 'TokenExpiredError') {
          return reject(new HttpError(422, 'Email Verification Token expired'));
        }
        return reject(new HttpError(422, 'Invalid Verification Token'));
      }
      DB.User
        .findOne({ where: { id: decoded.id } })
        .then((user) => {
          if (userId !== user.id || token !== user.verificationToken) {
            return reject(new HttpError(422, 'Invalid Verification Token'));
          }
          return resolve();
        });
    });
  });
}

module.exports.genAccountToken = genAccountToken;
module.exports.verifyAccountToken = verifyAccountToken;
module.exports.genTokenFromId = genTokenFromId;