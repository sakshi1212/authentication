const { sendgridClient, sendgridDefaults } = require('../config/sendgrid');

const sendMail = ({
  to,
  subject,
  text,
  html,
}) => {
  const message = {
    to,
    subject,
    text,
    html,
    ...sendgridDefaults,
  }

  console.log(message);

  return new Promise((resolve, reject) => {
    sendgridClient.send(message, (error, info) => {
      if (error) {
        console.log(error);
        reject(error);
      } else {
        resolve(info)
      }
    })
  });
};

module.exports = {
  sendMail,
};


