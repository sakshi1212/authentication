const asyncMW = require('../../../middleware/async');
const authenticate = require('../../../services/authenticate');
const bcryptService = require('../../../services/bcrypt');


exports.register = asyncMW(async (req, res) => {
  const {
    confirmPassword,
    ...user
  } = req.body;
  const transaction = await sequelize.transaction();

  try {
    const createdUser = await DB.User.create({
      ...user,
    }, { confirmPassword, transaction });

    const verificationTokenGeneratedAt = Date.now();
    const verificationToken = authenticate.genAccountToken({ id: createdUser.id });

    await createdUser.update({
      verified: false,
      verificationToken,
      verificationTokenGeneratedAt,
    }, { transaction });

    await createdUser.sendAccountVerificationEmail();
    console.log('===== VERIFICATION LINK ====', createdUser.verificationLink);

    await transaction.commit();
    return res.status(200).send({
      data: {
        ...createdUser.toPublic(),
      },
    });
  } catch (err) {
    transaction.rollback();
    throw err;
  }
});


exports.login = asyncMW(async (req, res) => {
  const {
    email,
    password,
  } = req.body;

  try {
    const user = await DB.User.findOne({
      where: {
        email,
      },
      rejectOnEmpty: new HttpError(404, 'Email not found'),
    });

    const passwordMatch = await bcryptService.comparePassword(password, user.password);
    if (!passwordMatch) {
      throw new HttpError(401, 'Password is incorrect. Please make sure you have entered correct information.');
    }

    const newToken = authenticate.genTokenFromId(user.id);

    return res.status(200).send({
      data: {
        ...user.toPublic(),
        token: newToken,
      },
    });
  } catch (err) {
    throw err;
  }
});

exports.verifyEmail = asyncMW(async (req, res) => {
  const { id } = req.params;
  const { token: verificationToken } = req.body;

  const transaction = await sequelize.transaction();
  try {
    const user = await DB.User.findOne({
      where: {
        id,
      },
    });
    
    if (!user) {
      throw new HttpError(404, `User with id ${id} not found`);
    }

    if (user.verified) {
      throw new HttpError(401, `Your account is already verified`);
    }

    await authenticate.verifyAccountToken(user.id, verificationToken);

    await user.update({
      verified: true,
      verificationToken: null,
      verificationTokenGeneratedAt: null,
      verifiedAt: Date.now(),
    }, { transaction });

    await transaction.commit();
    return res.status(200).send({
      data: {
        ...user.toPublic(),
      },
    });
  } catch (err) {
    transaction.rollback();
    throw err;
  }
});


