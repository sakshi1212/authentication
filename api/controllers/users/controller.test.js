const isObject = require('lodash/isObject');
const isInteger = require('lodash/isInteger');
const controller = require('./controller');
const mockHttp = require('../../../test/utils/http/mock');

let newOrderId;

describe('API: User in DB: Create', () => {
  test('Create a user in DB', async (done) => {
   const emailID = "sakshikakkar3@gmail.com"
    const { req, res, next } = mockHttp({
      request: {
        body: {
          "firstName": "Sakshi",
          "lastName": "Kakkar",
          "email": emailID,
          "password": "123123123",
          "confirmPassword": "123123123",
        }
      },
    });
    const { _status, _payload } = await controller.register(req, res, next);

    const { firstName, lastName, email, verified, id } = _payload.data;

    expect(_status).toBe(200);
    expect(isObject(_payload)).toBe(true);
    expect(isInteger(id)).toBe(true);
    expect(firstName).toBe("Sakshi");
    expect(lastName).toBe("Kakkar");
    expect(email).toBe(emailID);
    expect(verified).toBe(false);
    done();
  });

  test('Create a 2 users with same email in DB', async (done) => {
    const emailID = "sakshikakkar7@gmail.com"
     const { req, res, next } = mockHttp({
       request: {
         body: {
           "firstName": "Sakshi",
           "lastName": "Kakkar",
           "email": emailID,
           "password": "123123123",
           "confirmPassword": "123123123",
         }
       },
     });
     await controller.register(req, res, next);
     await controller.register(req, res, next);
     expect(next.mock.calls.length).toBe(1);
    //  expect(next.mock.calls[0][0].status).toBe(400);
     done();
   });

   test('Create a user and verify link', async (done) => {
    const emailID = "sakshikakkar26@gmail.com"
     const { req, res, next } = mockHttp({
       request: {
         body: {
           "firstName": "Sakshi",
           "lastName": "Kakkar",
           "email": emailID,
           "password": "123123123",
           "confirmPassword": "123123123",
         }
       },
     });
    const { _status, _payload } = await controller.register(req, res, next);

    const { verificationToken, id } = _payload.data;

    const { req: req2, res: res2, next: next2 } = mockHttp({
      request: {
        params: {
          "id": `${id}`,
        },
        body: {
          "token": `${verificationToken}`,
        }
      },
    });
    const response = await controller.verifyEmail(req2, res2, next2);
    const { _status: _status2, _payload: _payload2 } = response;
    const { verified: verifiedAfterVerification, verificationToken: tokenAfterVerification, id: userId } = _payload2.data;

    expect(_status2).toBe(200);
    expect(isObject(_payload2)).toBe(true);
    expect(isInteger(userId)).toBe(true);
    expect(userId).toBe(id);
    expect(verifiedAfterVerification).toBe(true);
    expect(tokenAfterVerification).toBe(null);
    done();
  });


  test('Verify Link of non existant user', async (done) => {
    const { req, res, next } = mockHttp({
      request: {
        params: {
          "id": "100000000"
        },
        body: {
          "token": "1234"
        }
      },
    });
    await controller.verifyEmail(req, res, next);
    expect(next.mock.calls.length).toBe(1);
    done();
  });

  test('Login User', async (done) => {
    const emailID = "sakshikakkar21@gmail.com"
      const { req, res, next } = mockHttp({
        request: {
          body: {
            "firstName": "Sakshi",
            "lastName": "Kakkar",
            "email": emailID,
            "password": "123123123",
            "confirmPassword": "123123123",
          }
        },
      });
    await controller.register(req, res, next);
    
    const { req: req2, res: res2, next: next2 } = mockHttp({
      request: {
        body: {
          "email": emailID,
          "password": "123123123",
        }
      },
    });

    const { _status, _payload } = await controller.login(req2, res2, next2);
    const { email, id } = _payload.data;

    expect(_status).toBe(200);
    expect(isObject(_payload)).toBe(true);
    expect(isInteger(id)).toBe(true);
    expect(email).toBe(emailID);
    
    done();
  });

});