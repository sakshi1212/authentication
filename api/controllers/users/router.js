const express = require('express');
const controller = require('./controller');

const router = express.Router();

router.post('/sign-up', controller.register);
router.post('/login', controller.login);
router.post('/:id/verify-email', controller.verifyEmail);

module.exports = router;
