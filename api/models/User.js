const Sequelize = require('sequelize');
const omit = require('lodash/omit');
const get = require('lodash/get');
const bcryptService = require('../../services/bcrypt');
const mailer = require('../../services/mailer');
const config = require('../../config/environment');

const { DataTypes } = Sequelize;

class User extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        email: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true,
          validate: {
            isEmail: {
              args: true,
              msg: 'Email is invalid.',
            },
            isUnique(value, next) {
              DB.User.findOne({ where: { email: value } })
                .done((user) => {
                  if (user) {
                    next(new HttpError(409, 'Email already exists!', 30003));
                  }

                  next();
                });
            },
          },
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        firstName: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        lastName: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        name: {
          type: DataTypes.VIRTUAL,
          get() {
            return `${this.firstName} ${this.lastName}`;
          },
        },
        verified: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        verifiedAt: {
          type: DataTypes.DATE,
        },
        verificationToken: {
          type: DataTypes.STRING,
        },
        verificationTokenGeneratedAt: {
          type: DataTypes.DATE,
        },
        verificationLink: {
          type: DataTypes.VIRTUAL,
          get() {
            return `${config.adminUrl}/verify-email?token=${this.verificationToken}&userId=${this.id}`;
          },
        }
      },
      {
        sequelize,
        modelName: 'users',
        scopes: {
        },
        hooks: {
          beforeCreate: (user, options) => {
            this.validatePasswordLength(user);
            this.validatePasswordConfirmation(user, options);
            return this.bcryptPasswordIfProvided(user)
              .then(bycryptedPassword => {
                user.password = bycryptedPassword;
              });
          },
          beforeUpdate: (user, options) => {
            const changedAttributes = user.changed();
            if (changedAttributes.includes('password')) {
              this.validatePasswordLength(user);
              this.validatePasswordConfirmation(user, options);
              return this.bcryptPasswordIfProvided(user)
                .then(bycryptedPassword => {
                  user.password = bycryptedPassword;
                });
            }
          },
        },
      }
    );
  }

  static associate(models) {
  }

  static isMine(idPath) {
    return req => new Promise((resolve, reject) =>
      this.findById(get(req, idPath, null), req.query)
        .then((user) => {
          if (req.user.id === user.id) {
            return resolve({ message: 'User.isMine success' });
          }
          return reject(new Error('User.isMine error'));
        })
        .catch(reject));
  }

  static validatePasswordLength(params) {
    const password = get(params, 'password', null);
    if (password && password.length < 6) {
      throw new HttpError(422, 'Password needs to be at least 7 characters long', 30002);
    }
    return params;
  }

  static validatePasswordConfirmation(params, { confirmPassword }) {
    const password = get(params, 'password', null);
    if (password && password !== confirmPassword) {
      throw new HttpError(422, 'Password and Password Confirmation do not match', 30002);
    }
    return params;
  }

  static async bcryptPasswordIfProvided(params) {
    const password = get(params, 'password');
    if (password) {
      const bycryptedPassword = await bcryptService.encryptPassword(password);
      return bycryptedPassword;
    }
    return password;
  }

  async sendAccountVerificationEmail() {
    const msg = {
      to: this.email,
      subject: 'Verify Your Email with ABC',
      text: `Click on ${this.verificationLink} to verify your account`,
      html: `<p>Click on ${this.verificationLink} to verify your account</p>`,
    };
    const result = await mailer.sendMail(msg);
    return result;
  }

  toPublic() {
    const {
      ...user
    } = omit(this.toJSON(), 'password');
    return {
      ...user,
    };
  }
}

module.exports = User;
