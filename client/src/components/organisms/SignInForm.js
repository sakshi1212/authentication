import React, { Component } from 'react';
import { Form, Message } from 'semantic-ui-react'
import axios from 'axios';
import isEmpty from 'lodash/isEmpty';
import isNull from 'lodash/isNull';
import every from 'lodash/every';
import get from 'lodash/get';

import Button from '../atoms/Button';
import { setUserInLocalStorage } from '../../services/authentication'


const formValidations = {
  email: 'Please enter your email',
  password: 'Please enter your password',
}

class SignInForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      email: '',
      password: '',
      emailError: null,
      passwordError: null,
      formError: null,
      formWarning: null,
      formWarningMessage: '',
      loading: false,
      submitDisabled: false,
      submittedOnce: false,
      verifiedEmail: get(this.props, 'location.state.verified', null),
      formErrorMessage: get(this.props, 'location.state.error', ''),
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  errorHandling = { 
    pointing: 'above'
  };

  handleChange = (e, {name, value}) => {
    this.setState({ 
      [name]: value,
      formWarning: null,
      formError: null,
      formErrorMessage: '',
      formWarningMessage: '',
    });
    if (this.state.submittedOnce) {
      this.validateForm();
    }
  }

  checkSubmitButtonState = () => {
    const errorsArr = Object.keys(formValidations).map(v => this.state[`${v}Error`]);
    if (every(errorsArr, isNull)) {
      this.setState({
        submitDisabled: false,
      });
    }
  }

  validateForm = () => {
    Object.keys(formValidations).forEach(key => {
      if (isEmpty(this.state[key])){
        this.setState({ 
          [`${key}Error`]: { content: formValidations[key], ...this.errorHandling }, 
          submitDisabled: true
        }, this.checkSubmitButtonState);
      } else {
        this.setState({
          [`${key}Error`]: null,
        }, this.checkSubmitButtonState);
      }
    })
  }

  handleSubmit = () => {
    this.validateForm();

    this.setState({
      loading: true,
      verifiedEmail: null,
    });
    if (!this.state.submittedOnce) {
      this.setState({
        submittedOnce: true,
      });
    }

    const {
      email,
      password,
    } = this.state;

    axios.post('/users/login', {
      email,
      password
    })
    .then(({ data: user }) => {
      this.setState({
        loading: false,
        formSuccess: true,
        email: '',
        password: '',
        submittedOnce: false,
      });
      const verified = get(user, 'data.verified', false);
      if (verified) {
        setUserInLocalStorage(user);
        this.props.history.push('/dashboard');
      } else {
        this.setState({
          formWarning: true,
          formWarningMessage: 'You do have an account with us, Please click on the verification link on your email to continue'
        })
      }
    })
    .catch((err) => {
      this.setState({
        formError: true,
        formErrorMessage: `${err}`,
        loading: false,
      });
    });
  }

  render() {
    return (
      <Form 
        success={this.state.verifiedEmail}
        warning={this.state.formWarning}
        error={this.state.formError}
        loading={this.state.loading}
        onSubmit={this.handleSubmit}>
        <Message
          error
          header='There was an error !'
          content={this.state.formErrorMessage || false}
        />
        <Message
          success
          header='Verification Successfull'
          content={'Please sign in to continue'}
        />
        <Message
          warning
          header='You cannot sign in !'
          content={this.state.formWarningMessage || false}
        />
        <Form.Input
          placeholder='Email'
          name='email'
          value={this.props.email}
          onChange={this.handleChange}
          error={this.state.emailError || false}
          icon="mail"
          iconPosition="left"
        />
        <Form.Input
          placeholder='Password'
          name='password'
          value={this.props.password}
          onChange={this.handleChange}
          error={this.state.passwordError || false}
          icon="caret right"
          iconPosition="left"
          type="password"
        />
        <Button 
          type='submit'
          disabled={this.state.submitDisabled}
        >
          Submit
        </Button>
      </Form>
    );
  }
}
export default SignInForm;
