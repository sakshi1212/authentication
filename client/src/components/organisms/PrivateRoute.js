import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { getUserFromLocalStorage } from '../../services/authentication';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => {
      const currentUser = getUserFromLocalStorage();
      if (!currentUser) {
        return <Redirect to={{ pathname: '/sign-in', state: { from: props.location } }} />
      }
      return <Component {...props} />
  }} />
)

export default PrivateRoute;