import React, { Component } from 'react';
import { Form, Message } from 'semantic-ui-react'
import axios from 'axios';
import Button from '../atoms/Button';
import isEmpty from 'lodash/isEmpty';
import isNull from 'lodash/isNull';
import every from 'lodash/every';

const formValidations = {
  firstName: 'Please enter your first name',
  lastName: 'Please enter your last name',
  email: 'Please enter your email',
  password: 'Please enter your password',
  confirmPassword: 'Please re-enter your password for confirmation',
}

class SignUpForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirmPassword: '',
      firstNameError: null,
      lastNameError: null,
      emailError: null,
      passwordError: null,
      confirmPasswordError: null,
      loading: false,
      submitDisabled: false,
      submittedOnce: false,
      formSuccess: null,
      formError: null,
      formErrorMessage: ''
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  errorHandling = { 
    pointing: 'above'
  };

  handleChange = (e, {name, value}) => {
    this.setState({ 
      [name]: value,
      formSuccess: null,
      formError: null,
      formErrorMessage: '',
    });
    if (this.state.submittedOnce) {
      this.validateForm();
    }
  }

  checkSubmitButtonState = () => {
    const errorsArr = Object.keys(formValidations).map(v => this.state[`${v}Error`]);
    if (every(errorsArr, isNull)) {
      this.setState({
        submitDisabled: false,
      });
    }
  }

  isNotEmail = (value) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return !re.test(value);
  };

  validateForm = () => {
    Object.keys(formValidations).forEach(key => {
      if (isEmpty(this.state[key])){
        this.setState({ 
          [`${key}Error`]: { content: formValidations[key], ...this.errorHandling }, 
          submitDisabled: true
        }, this.checkSubmitButtonState);
      } else if(key === 'password' && this.state.password.length<7) {
        this.setState({ 
          [`${key}Error`]: { content: 'Password has to be atleast 7 chars', ...this.errorHandling }, 
          submitDisabled: true
        }, this.checkSubmitButtonState);
      } else if(key === 'email' && this.isNotEmail(this.state.email)) {
        this.setState({ 
          [`${key}Error`]: { content: 'Please enter valid email', ...this.errorHandling }, 
          submitDisabled: true
        }, this.checkSubmitButtonState);
      } else {
        this.setState({
          [`${key}Error`]: null,
        }, this.checkSubmitButtonState);
      }
    })
  }

  handleSubmit = () => {
    this.setState({
      loading: true,
    });
    if (!this.state.submittedOnce) {
      this.setState({
        submittedOnce: true,
      });
    }

    const {
      firstName,
      lastName,
      email,
      password,
      confirmPassword,
    } = this.state;

    this.validateForm();

    axios.post('/users/sign-up', {
      firstName,
      lastName,
      email,
      password,
      confirmPassword,
    })
    .then(({ data }) => {
      this.setState({
        loading: false,
        formSuccess: true,
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        confirmPassword: '',
        submittedOnce: false,
      });
    })
    .catch((err) => {
      this.setState({
        formError: true,
        formErrorMessage: `${err}`,
        loading: false,
      });
    });
  }

  render() {
    return (
      <Form
        success={this.state.formSuccess}
        error={this.state.formError}
        loading={this.state.loading}
        onSubmit={this.handleSubmit}>
        <Message
          error
          header='There was an error !'
          content={this.state.formErrorMessage || false}
        />
        <Form.Input
          placeholder='First Name'
          name='firstName'
          onChange={this.handleChange}
          error={this.state.firstNameError || false}
          value={this.state.firstName}
          icon="user"
          iconPosition="left"
        />
        <Form.Input
          placeholder='Last Name'
          name='lastName'
          onChange={this.handleChange}
          error={this.state.lastNameError || false}
          value={this.state.lastName}
          icon="user"
          iconPosition="left"
        />
        <Form.Input
          placeholder='Email'
          name='email'
          onChange={this.handleChange}
          error={this.state.emailError || false}
          value={this.state.email}
          icon="mail"
          iconPosition="left"
        />
        <Form.Input
          placeholder='Password'
          name='password'
          onChange={this.handleChange}
          error={this.state.passwordError || false}
          value={this.state.password}
          icon="caret right"
          iconPosition="left"
          type="password"
        />
        <Form.Input
          placeholder='ConfirmPassword'
          name='confirmPassword'
          onChange={this.handleChange}
          error={this.state.confirmPasswordError || false}
          value={this.state.confirmPassword}
          icon="caret right"
          iconPosition="left"
          type="password"
        />
        <Button 
          type='submit'
          disabled={this.state.submitDisabled}
        >
          Submit
        </Button>
        <Message
          success
          header='Sign Up Successfull !'
          content="You're all signed up for the our website. Please check your inbox for a verification email"
        />
      </Form>
    );
  }
}
export default SignUpForm;
