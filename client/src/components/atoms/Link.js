import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import NavLink from 'react-router-dom/NavLink';

const styles = css`
  text-decoration: none;
  transition: all 0.1s ease;
  &:hover,
  &:focus {
    text-decoration: 'underline';
  }
`;

const StyledNavLink = styled(({ theme, reverse, palette, ...props }) => (
  <NavLink {...props} />
))`
  ${styles};
`;

const Anchor = styled.a`
  ${styles};
`;

const Link = ({ ...props }) => {
  const { disabled } = props;
  if (disabled) { return <Anchor {...props} />; }

  if (props.to) {
    return <StyledNavLink {...props} />;
  }
  return <Anchor {...props} />;
};

Link.propTypes = {
  disabled: PropTypes.bool,
  palette: PropTypes.string,
  reverse: PropTypes.bool,
  to: PropTypes.string,
};

Link.defaultProps = {
  palette: 'primary',
};

export default Link;
