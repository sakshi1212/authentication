import React, { Component } from 'react';
import Button from '../../components/atoms/Button';
import { removeUserfromLocalForage } from '../../services/authentication';

class Dashboard extends Component {
  constructor(props){
    super(props);
    this.state = {
    }
    this.logoutUser = this.logoutUser.bind(this)
  }

  componentDidMount() {
  }

  logoutUser () {
    removeUserfromLocalForage();
    this.props.history.push('/login');
  }

  render() {
    return (
      <div className="App">
        <h1>Welcome ! You are now signed in</h1>
        <Button 
          onClick={this.logoutUser}
        >
          Sign out !
        </Button>
      </div>
    );
  }
}

export default Dashboard;