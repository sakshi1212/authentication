import React, { Component } from 'react';
import SignUpForm from '../../components/organisms/SignUpForm';
import Link from '../../components/atoms/Link';

class Home extends Component {
  render() {
    return (
    <div className="App">
      <h1>Sign Up</h1>
      <SignUpForm />
      <Link to={'/login'}>Already have an account? Sign In</Link>
    </div>
    );
  }
}
export default Home;