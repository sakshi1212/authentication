import React, { Component } from 'react';
// import SignInForm from '../../components/organisms/SignInForm';
// import Link from '../../components/atoms/Link';
import axios from 'axios';
import qs from 'qs';

class VerifyEmail extends Component {
  constructor(props){
    super(props);
    this.state = {
      verificationSuccessFul: false,
    }
  }
  componentDidMount() {
    const {
      location,
    } = this.props;
    if (location.search) {
      const query = qs.parse(location.search, { ignoreQueryPrefix: true });
      const {
        userId,
        token,
      } = query;
      axios.post(`/users/${userId}/verify-email`, { token })
        .then((resp) => {
          this.props.history.push('/login', { verified: true })
        })
        .catch(error => {
          console.log(error);
          this.props.history.push('/login', { verified: false, error: error })
        });
    }
  }

  render() {
    return (
    <div className="App">
      <h1>Verifying Email</h1>
    </div>
    );
  }
}
export default VerifyEmail; 