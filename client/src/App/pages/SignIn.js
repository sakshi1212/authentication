import React, { Component } from 'react';
import SignInForm from '../../components/organisms/SignInForm';
import Link from '../../components/atoms/Link';

class Home extends Component {
  render() {
    return (
    <div className="App">
      <h1>Sign In</h1>
      <SignInForm {...this.props} />
      <Link to={'/sign-up'}>Dont Have an Account? Sign Up</Link>
    </div>
    );
  }
}
export default Home; 