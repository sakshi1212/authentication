import React, { Component, Fragment } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import styled from 'styled-components';
import './App.css';
import Home from './pages/Home';
import SignIn from './pages/SignIn';
import Dashboard from './pages/Dashboard';
import VerifyEmail from './pages/VerifyEmail';
import PrivateRoute from '../components/organisms/PrivateRoute';

const AppContent = styled.div`
  min-height: calc(90vh);
  display: flex;
  flex-direction: column;
  min-width: 375px;
  margin: 0 auto;
  background: aliceblue;
  padding: 50px 20%;
  @media (min-width: 640px) {
  }
`

class App extends Component {
  render() {
    const App = () => (
      <Fragment>
        <AppContent>
          <Switch>
            <Route exact path='/sign-up' component={Home}/>
            <Route path='/verify-email' component={VerifyEmail}/>
            <Route path='/login' component={SignIn}/>
            <PrivateRoute path='/dashboard' component={Dashboard}/>
            <Route exact path="/">
              <Redirect to="/sign-up" />
            </Route>
          </Switch>
        </AppContent>
        {/* <Footer/> */}
      </Fragment>
    )
    return (
      <Switch>
        <App/>
      </Switch>
    );
  }
}

export default App;