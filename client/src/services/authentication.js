const setUserInLocalStorage = (data) => {
  localStorage.setItem('currentUser', JSON.stringify(data));
}

const getUserFromLocalStorage = () => {
  const currentUserSubject = JSON.parse(localStorage.getItem('currentUser'));
  return currentUserSubject;
}

const removeUserfromLocalForage = () => {
  localStorage.removeItem('currentUser');
}

module.exports = {
  setUserInLocalStorage,
  getUserFromLocalStorage,
  removeUserfromLocalForage
}