const config = require('./environment');

const sendgridClient = require('@sendgrid/mail');
sendgridClient.setApiKey(config.sendgrid.key);

const sendgridDefaults = {
  subject: 'Message from ABC Team',
  from: 'skakkar121291@gmail.com',
};

module.exports = {
  sendgridClient,
  sendgridDefaults,
};

