'use strict';

const path = require('path');
const _ = require('lodash');
const fs = require('fs');

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  try {
    fs.accessSync(path.normalize(`${__dirname}/default-env.js`), fs.F_OK);
    require('./default-env');
  } catch (e) {
    console.warn('You are in development, but default-env.js does not exist. Please set up default-env');
  }
}

let defaultConfig = {
  env: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 8080,
  adminUrl: process.env.ADMIN_URL || 'http://localhost:3000',
  mysql: {
    name: process.env.DB_NAME,
    host: process.env.DB_HOST,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
  },
  secrets : {
    token: process.env.SECRET_TOKEN || 'pinkpanther'
  },
  expireTimes: {
    token: process.env.TOKEN_EXPIRE_TIME || 7 * 24 * 60 * 60
  },
  sendgrid: {
    key: process.env.SENDGRID_KEY,
  }
};

// merge defaultconfig with environment specific config
defaultConfig = _.merge(
  defaultConfig,
  require(`${__dirname}/${defaultConfig.env}.js`) || {} 
);

// console.log(defaultConfig);

module.exports = defaultConfig;
