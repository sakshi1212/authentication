'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('users', 'verified', {
        type: Sequelize.BOOLEAN,
      }),
      queryInterface.addColumn('users', 'verificationToken', {
        type: Sequelize.STRING,
      }),
      queryInterface.addColumn('users', 'verificationTokenGeneratedAt', {
        type: Sequelize.DATE,
      }),
      queryInterface.addColumn('users', 'verifiedAt', {
        type: Sequelize.DATE,
      }),
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('users', 'verified'),
      queryInterface.removeColumn('users', 'verificationToken'),
      queryInterface.removeColumn('users', 'verificationTokenGeneratedAt'),
      queryInterface.removeColumn('users', 'verifiedAt'),
    ]);
  },
};
